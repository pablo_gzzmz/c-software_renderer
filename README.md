# README #
#
Author: Pablo Gonz�lez Mart�nez
#
pablogonzmartinez@gmail.com

Visual Studio 2015, C++

This project showcases the implementation of a renderer by software, coded from scratch in Visual Studio, which can load .obj files and rasterize their triangles on screen.

# Google Docs doc #
https://docs.google.com/document/d/1XtR88EYaGVZ5LD-qhG9pjb-cuSxsPxWVV0MFxP9ShHw/edit?usp=sharing

# After-download guide: #
It contains no .exe, as the execution of the project itself ignoring the code is not very relevant.

It should be opened in Visual Studio via the direct access contained in the base folder; otherwise, go to Software_Renderer -> Projects -> vs-2015 -> Software_Renderer

To test the results, it should be debugged in Release mode, otherwise Debug�s overhead heavily slows down the execution.