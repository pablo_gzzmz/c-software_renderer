
#include "Screen.hpp"

namespace engine {
	size_t Screen::width  = 1850;
	size_t Screen::height = 1000;

	Color_Buffer_Rgba8888             Screen::color_buffer           = Color_Buffer_Rgba8888(width, height);
	Rasterizer<Color_Buffer_Rgba8888> Screen::rasterizer = Rasterizer<Color_Buffer_Rgba8888>(color_buffer);

	Scene_Render_Info Screen::render_info = Scene_Render_Info
	(
		// Hardcoded simple directional light - already inverted and normalized
		Vector3f({ 0.5f, 0.6f, -0.6f }),

		// Hardcoded default ambient color for the whole scene
		Color(110, 120, 190, 255)
	);

	void Screen::set_screen(size_t _width, size_t _height) {
		width  = _width;
		height = _height;

		color_buffer = Color_Buffer_Rgba8888(width, height);
		rasterizer.initialize(color_buffer);
	}

	void Screen::set_render_info(const Vector3f _directional_light, const Color _ambient_color) {
		render_info.light         = _directional_light;
		render_info.ambient_color = _ambient_color;
	}
}