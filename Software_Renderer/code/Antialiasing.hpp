

/*
	Author: Pablo Gonzalez Martinez

	Antialiasing class:
		- This class simulates a postprocessing anti-aliasing through edge detection and gaussian blur
		- The class does NOT have any actual anti-alias behaviour
		- The edge detection is based on color contrast with neighbouring pixels
		- The gaussian blur is applied to those pixels who pass the threshold of contrast and are considered edges
*/

#ifndef ANTIALIASING_HEADER
#define ANTIALIASING_HEADER

#include "Screen.hpp"

namespace engine {

	class Antialiasing {
	private:
		const float edge_threshold = 50.f;

		// Mask for pixel neighbours
		std::vector<Color> mask;
		// Mask for diagonal pixel neighbours
		std::vector<Color> diagonal_mask;
		size_t mask_count;

		std::vector<Color> swap_buffer;

		static bool debugging_edges;

		enum pixel_hztl_pos {
			LEFT_BORDER,
			RIGHT_BORDER,
			HZTL_DEFAULT
		};

		enum pixel_vtcl_pos {
			TOP_BORDER,
			BOTTOM_BORDER,
			VTCL_DEFAULT
		};

	private:
		Antialiasing() : mask(4), diagonal_mask(4), swap_buffer(Screen::get_width() * Screen::get_height()) {
			mask_count  = 0;
		}

		int color_diff(Color c1, Color c2) {
			return abs(c1.R() - c2.R()) + abs(c1.G() - c2.G()) + abs(c1.B() - c2.B());
		}

		float max(float a, float b) {
			return a < b ? b : a;
		}

		float min(float a, float b) {
			return a < b ? a : b;
		}

	public:
		static Antialiasing instance() {
			static Antialiasing antialiasing = Antialiasing();
			return antialiasing;
		}

		static void debugging_switch() {
			debugging_edges = !debugging_edges;
		}

		void pass();
	};
}


#endif