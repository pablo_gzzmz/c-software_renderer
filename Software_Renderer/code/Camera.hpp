
/*
	Author: Pablo Gonzalez Martinez

	Camera class:
		- A camera is in charge of rendering models
		- There is a static vector of existing models to be rendered
		- The camera class will contain a static vector to all existing cameras
		- It will hold information of active camera, labelled as main
*/

#ifndef CAMERA_HEADER
#define CAMERA_HEADER

#include "Projection.hpp"
#include "Model.hpp"
#include "Screen.hpp"
#include "Antialiasing.hpp"

namespace engine {
	using toolkit::Projection3f;
	using toolkit::Transformation3f;

	class Camera : public Entity{
	private:
		Projection3f projection;

		float near_z;
		float far_z;
		float fov;

		static vector<Camera*> cameras;
		static uint32_t main;
		uint32_t index;

		static vector<Model*> models;

		bool display_aliasing;

	public:
		Camera(float _near_z, float _far_z, float _fov);

		static void add_model(Model * new_model) {
			models.push_back(new_model);
		}

		static bool set_main(uint32_t i) {
			if (i < cameras.size()) {
				main = i;
				return true;
			}
			return false;
		}

		static uint32_t consult_main() { return main; }

		static Camera * get_main() {
			return cameras[main];
		}

		const Transformation3f get_projection() const { return projection; }

		void set_display_aliasing(bool b) {
			display_aliasing = b;
		}

		void render();
	};
}

#endif

