
#include "Scene.hpp"

namespace engine{
	void Scene::update(float t) {
		for (auto it = entities.begin(); it != entities.end(); it++) {
				(*it)->update(t, Camera::get_main()->get_projection());
		}
	}
}