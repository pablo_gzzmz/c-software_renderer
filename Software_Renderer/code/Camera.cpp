
#include "Camera.hpp"

namespace engine {
	vector<Camera*> Camera::cameras	= vector<Camera*>();
	
	uint32_t Camera::main = 0;

	vector<Model*> Camera::models = vector<Model*>();

	Camera::Camera(float _near_z, float _far_z, float _fov)
		:
		projection(_near_z, _far_z, _fov, float(Screen::get_width()) / float(Screen::get_height())
		) {
		index = cameras.size();
		cameras.push_back(this);
		near_z = _near_z;
		far_z = _far_z;
		fov = _fov;

		display_aliasing = false;
	}


	void Camera::render() {
		Screen::clear();

		for (auto it = models.begin(); it != models.end(); it++) {
			(*it)->render(Screen::get_width(), Screen::get_height(), Screen::get_rasterizer(), Screen::get_info());
		}

		if (display_aliasing)
			Antialiasing::instance().pass();

		Screen::draw();
	}
}