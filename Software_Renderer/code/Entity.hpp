

/*
	Author: Pablo Gonzalez Martinez

	Entity base class:
		- An entity is any object that will exist in the scene
		- Entities contain transforms which can be updated / edited
*/

#ifndef ENTITY_HEADER
#define ENTITY_HEADER

#include <map>
#include <string>
#include <Scaling.hpp>
#include <Rotation.hpp>
#include <Translation.hpp>
#include <memory>

namespace engine {
	using std::map;
	using std::string;
	using std::shared_ptr;
	using toolkit::Transformation3f;
	using toolkit::Scaling3f;
	using toolkit::Rotation3f;
	using toolkit::Translation3f;

	class Entity {
	protected:
		map<string, Entity*> children;
		
		float scale;
		float rotation_x;
		float rotation_y;
		float rotation_z;
		float translation_x;
		float translation_y;
		float translation_z;
		
		Transformation3f transform;

	public:
		Entity();

		Entity(float _scale,
			   float _rotation_x,    float _rotation_y,    float _rotation_z,
			   float _translation_x, float _translation_y, float _translation_z);

		virtual void update(float t, const Transformation3f & parent_transform) {
			Scaling3f	  scaling_trnsfm(scale);
			Rotation3f    rotation_trnsfm_x;
			Rotation3f    rotation_trnsfm_y;
			Rotation3f	  rotation_trnsfm_z;
			rotation_trnsfm_x.set<Rotation3f::AROUND_THE_X_AXIS>(rotation_x);
			rotation_trnsfm_y.set<Rotation3f::AROUND_THE_Y_AXIS>(rotation_y);
			rotation_trnsfm_z.set<Rotation3f::AROUND_THE_Z_AXIS>(rotation_z);
			Translation3f translation_trnsfm(translation_x, translation_y, translation_z);

			transform = parent_transform * translation_trnsfm * rotation_trnsfm_x * rotation_trnsfm_y * rotation_trnsfm_z * scaling_trnsfm;

			for (auto it = children.begin(); it != children.end(); it++) {
				it->second->update(t, transform);
			}
		}

		void add_child(Entity * new_child, string name) {
			children[name] = new_child;
		}

		#pragma region Transform setting
		void set_scale(float _scale) {
			scale = _scale;
		}
		void set_rotation(float _rotation_x, float _rotation_y, float _rotation_z) {
			rotation_x = _rotation_x;
			rotation_y = _rotation_y;
			rotation_z = _rotation_z;
		}

		void set_rotation_x(float _rotation_x) {
			rotation_x = _rotation_x;
		}

		void set_rotation_y(float _rotation_y) {
			rotation_y = _rotation_y;
		}

		void set_rotation_z(float _rotation_z) {
			rotation_z = _rotation_z;
		}

		void set_translation(float _translation_x, float _translation_y, float _translation_z) {
			translation_x = _translation_x;
			translation_y = _translation_y;
			translation_z = _translation_z;
		}

		void set_translation_x(float _translation_x) {
			translation_x = _translation_x;
		}

		void set_translation_y(float _translation_y) {
			translation_y = _translation_y;
		}
		
		void set_translation_z(float _translation_z) {
			translation_z = _translation_z;
		}
		#pragma endregion

		#pragma region Transform getting
		float get_scale() { return scale; }

		float get_rotation_x() { return rotation_x; }
		float get_rotation_y() { return rotation_y; }
		float get_rotation_z() { return rotation_z; }

		float get_translation_x() { return translation_x; }
		float get_translation_y() { return translation_y; }
		float get_translation_z() { return translation_z; }
		#pragma endregion

		#pragma region Transform addition
		void add_scale(float addition) {
			scale += addition;
		}

		void add_rotation(float addition_x, float addition_y, float addition_z) {
			rotation_x += addition_x;
			rotation_y += addition_y;
			rotation_z += addition_z;
		}

		void add_rotation_x(float addition) {
			rotation_x += addition;
		}

		void add_rotation_y(float addition) {
			rotation_y += addition;
		}

		void add_rotation_z(float addition) {
			rotation_z += addition;
		}

		void add_translation(float addition_x, float addition_y, float addition_z) {
			translation_x += addition_x;
			translation_y += addition_y;
			translation_z += addition_z;
		}

		void add_translation_x(float addition) {
			translation_x += addition;
		}

		void add_translation_y(float addition) {
			translation_y += addition;
		}

		void add_translation_z(float addition) {
			translation_z += addition;
		}
		#pragma endregion
	};
}

#endif
