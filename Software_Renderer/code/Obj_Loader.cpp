
#include "Obj_Loader.hpp"
#include <assert.h>

namespace engine {
	using std::vector;

	bool load_obj(vector<Mesh> * meshes, const std::string & obj_file_path) {
		FILE * file = fopen(obj_file_path.c_str(), "r");

		assert(file != NULL);

		if (file == NULL) {
			printf("File is NULL \n");
			return false;
		}

		meshes->resize(1);
		size_t current_mesh = -1;

		while (1) {
			char line_header[128];
			// Read the first word of the line
			int res = fscanf(file, "%s", line_header);
			if (res == EOF) // Break out if end of line
				break; 

			if (strcmp(line_header, "object") == 0) {
				// Increase one mesh for every object contained in the .obj
				current_mesh++;
				meshes->resize(current_mesh+1);
			}

			// *************************************** COLOR

			// Get color for the whole mesh. 
			// This value is not an export default and should be manually added to the .obj file
			else if (strcmp(line_header, "color") == 0) {
				int r, g, b;
				fscanf(file, "%i %i %i\n", &r, &g, &b);
				r;
				g;
				b;
				(*meshes)[current_mesh].get_mesh_color().set(r, g, b);
			}


			// *************************************** VERTICES

			else if (strcmp(line_header, "v") == 0) {
				Point4f vertex;
				fscanf(file, "%f %f %f\n", &vertex[0], &vertex[1], &vertex[2]);
				vertex[3] = 1;
				(*meshes)[current_mesh].get_original_vertices().push_back(vertex);
			}


			// *************************************** TEXTURE COORDINATES

			else if (strcmp(line_header, "vt") == 0) {
				Point2f uv;
				fscanf(file, "%f %f\n", &uv[0], &uv[1]);
				(*meshes)[current_mesh].get_original_uvs().push_back(uv);
			}

			// *************************************** NORMALS

			else if (strcmp(line_header, "vn") == 0) {
				Point3f normal;
				fscanf(file, "%f %f %f\n", &normal[0], &normal[1], &normal[2]);
				(*meshes)[current_mesh].get_original_normals().push_back(normal);
			}


			// *************************************** INDICES

			else if (strcmp(line_header, "f") == 0) {
				// Get vertices, uv and normal indices for a face
				unsigned int vertex_index[3], uv_index[3], normal_index[3];
				int matches = fscanf(
					file, "%d/%d/%d %d/%d/%d %d/%d/%d\n",
					// Cambie el orden de los indices para que sea compatible con como me lo exporta 3Dmax
					&vertex_index[2], &uv_index[2], &normal_index[2],
					&vertex_index[1], &uv_index[1], &normal_index[1],
					&vertex_index[0], &uv_index[0], &normal_index[0]
				);
				(*meshes)[current_mesh].get_vertex_indices().push_back(vertex_index[0]-1);
				(*meshes)[current_mesh].get_vertex_indices().push_back(vertex_index[1]-1);
				(*meshes)[current_mesh].get_vertex_indices().push_back(vertex_index[2]-1);
				(*meshes)[current_mesh].get_uv_indices().push_back(uv_index[0]-1);
				(*meshes)[current_mesh].get_uv_indices().push_back(uv_index[1]-1);
				(*meshes)[current_mesh].get_uv_indices().push_back(uv_index[2]-1);
				(*meshes)[current_mesh].get_normal_indices().push_back(normal_index[0]-1);
				(*meshes)[current_mesh].get_normal_indices().push_back(normal_index[1]-1);
				(*meshes)[current_mesh].get_normal_indices().push_back(normal_index[2]-1);
			}
		}

		return true;
	}
}