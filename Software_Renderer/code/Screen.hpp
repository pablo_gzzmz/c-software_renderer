

/*
	Author: Pablo Gonzalez Martinez

	Screen class:
		- Class with static behaviour to portray Screen data
		- This class holds the main rasterizer, color buffer and render information
		- Should be accessed by other classes whom might need raster-related operations
*/

#ifndef SCREEN_HEADER
#define SCREEN_HEADER

#include "Rasterizer.hpp"
#include "Projection.hpp"
#include "Scene_Render_Info.hpp"
#include <vector>
#include <memory>

namespace engine {
	using toolkit::Vector3f;
	using toolkit::Projection3f;
	using std::vector;
	using std::shared_ptr;

	class Screen {
		static size_t width;
		static size_t height;

		static Color_Buffer_Rgba8888 color_buffer;
		static Rasterizer<Color_Buffer_Rgba8888> rasterizer;

		static Scene_Render_Info render_info;

	public:
		static void set_screen(size_t _width, size_t _height);
		static void set_render_info(const Vector3f _directional_light, const Color _ambient_color);

		static size_t get_width()  { return width;  }
		static size_t get_height() { return height; }

		static Rasterizer<Color_Buffer_Rgba8888> * get_rasterizer()	  { return & rasterizer;   }
		static Color_Buffer_Rgba8888			 * get_color_buffer() { return & color_buffer; }

		static const Scene_Render_Info & get_info() { return render_info; }

		static void clear() {
			rasterizer.clear();
		}

		static void draw() {
			color_buffer.gl_draw_pixels(0, 0);
		}
	};
}

#endif