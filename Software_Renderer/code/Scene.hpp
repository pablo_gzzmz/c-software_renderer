

/*
	Author: Pablo Gonzalez Martinez

	Scene class:
		- A scene contains a vector of entities that will be updated
		- The scene will pass the camera projection as the root transformation
*/

#ifndef SCENE_HEADER
#define SCENE_HEADER

#include <vector>
#include <memory>
#include "Camera.hpp"

namespace engine {
	using std::vector;
	using std::shared_ptr;

	class Scene {
	private:
		vector<Entity*> entities;

	public:
		void add_entity(Entity * new_entity){
			entities.push_back(new_entity);
		}

		void update(float t);
	};
}

#endif
