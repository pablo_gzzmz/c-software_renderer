
/*
	Author: Pablo Gonzalez Martinez

	Rasterizer class:
		- Rasterizes triangles based on established Color Buffer
		- Divides triangles in two flat triangles and rasterizes their scanlines
		- Uses z_buffer
		- Saves a buffer with painted pixels every frame
*/


#ifndef RASTERIZER_HEADER
#define RASTERIZER_HEADER

#include <limits>
#include <vector>
#include <stdint.h>
#include <Point.hpp>
#include <algorithm>
#include <math.h>

namespace engine {
	using toolkit::Point4i;
	using toolkit::Point4f;
	using std::vector;
	using std::numeric_limits;

	template<class COLOR_BUFFER_TYPE>
	class Rasterizer{
	public:
		typedef COLOR_BUFFER_TYPE            Color_Buffer;
		typedef typename Color_Buffer::Color Color;

	private:
		Color_Buffer & color_buffer;
		vector<int>    z_buffer;

		vector<size_t> pixel_buffer; // Buffer containing offset data of pixels rasterized in a frame
		size_t		   pixel_amount;

	public:
		Rasterizer() {}

		Rasterizer(Color_Buffer & target) : color_buffer(target), 
											z_buffer    (target.get_width() * target.get_height()),
											pixel_buffer(target.get_width() * target.get_height())
											{}

		void initialize(Color_Buffer & target) {
			color_buffer = Color_Buffer(target);
			z_buffer.resize(target.get_width() * target.get_height());
		}

		Color_Buffer & get_color_buffer() { return (color_buffer); }

		const vector<size_t> & get_pixel_buffer() { return pixel_buffer; }
		const size_t		   get_pixel_amount() { return pixel_amount; }

		void set_color(const Color & new_color){
			color_buffer.set_color(new_color);
		}

		void set_color(int r, int  g, int b){
			color_buffer.set_color(r, g, b);
		}

		void clear(){
			for (Color * c = color_buffer.colors(), *end = c + color_buffer.size(); c < end; c++)
				*c = 0;

			for (int * z = z_buffer.data(), *end = z + z_buffer.size(); z != end; z++)
				*z = numeric_limits<int>::max();

			pixel_amount = 0;
		}

		void swap_vertex(float & x0, float & y0, float & z0, float & x1, float & y1, float & z1);

		void raster_triangle(const Point4i * const vertices, const int * const indices);

	private:
		void raster_flat_top_triangle(
			float v0_x, float v0_y, float v0_z,
			float v1_x,				float v1_z,
			float v2_x, float v2_y, float v2_z
			);
		void raster_flat_bot_triangle(
			float v0_x, float v0_y, float v0_z,
			float v1_x, float v1_y, float v1_z,
			float v2_x,             float v2_z
		);

		void raster_scanline(const int y, int x0, int x1, float z0, float z1);

	}; // End of class

	template<class COLOR_BUFFER_TYPE>
	void Rasterizer<COLOR_BUFFER_TYPE>::swap_vertex(float & x0, float & y0, float & z0, float & x1, float & y1, float & z1) {
		float tempx = x0, tempy = y0, tempz = z0;

		x0 = x1;    y0 = y1;    z0 = z1;
		x1 = tempx; y1 = tempy; z1 = tempz;
	}


	template<class COLOR_BUFFER_TYPE>
	void Rasterizer<COLOR_BUFFER_TYPE>::raster_triangle(const Point4i * const vertices, const int * const indices) {
		// Cache vertex variables
		float v0_x = (float)vertices[indices[0]][0];
		float v0_y = (float)vertices[indices[0]][1];
		float v0_z = (float)vertices[indices[0]][2];
		float v1_x = (float)vertices[indices[1]][0];
		float v1_y = (float)vertices[indices[1]][1];
		float v1_z = (float)vertices[indices[1]][2];
		float v2_x = (float)vertices[indices[2]][0];
		float v2_y = (float)vertices[indices[2]][1];
		float v2_z = (float)vertices[indices[2]][2];

		// Arrange in order (up -> down)
		if (v0_y < v1_y) { swap_vertex(v0_x, v0_y, v0_z, v1_x, v1_y, v1_z); }
		if (v1_y < v2_y) { swap_vertex(v1_x, v1_y, v1_z, v2_x, v2_y, v2_z); }
		if (v0_y < v1_y) { swap_vertex(v0_x, v0_y, v0_z, v1_x, v1_y, v1_z); }

		// Cases with already flat triangles
		if (v1_y == v2_y) {
			// Ensure order of flat line vectors (left -> right)
			if (v1_x > v2_x) { swap_vertex(v1_x, v1_y, v1_z, v2_x, v2_y, v2_z); }

			raster_flat_bot_triangle(v0_x, v0_y, v0_z, v1_x, v1_y, v1_z, v2_x, v2_z);
		}
		else
		if (v0_y == v1_y) {
			// Ensure order of flat line vectors (left -> right)
			if (v0_x > v1_x) { swap_vertex(v0_x, v0_y, v0_z, v1_x, v1_y, v1_z); }

			raster_flat_top_triangle(v0_x, v0_y, v0_z, v1_x, v1_z, v2_x, v2_y, v2_z);
		}

		// Cases with split triangle
		else {
			float delta = (v1_y - v0_y) / (v2_y - v0_y);

			// Cache new vertex variables
			float v3_x = v0_x + (delta * (v2_x - v0_x)); // x3 = x0 + ((Dy01 / Dy12) * Dx02)
			float v3_y = v1_y;
			float v3_z = v0_z + (delta * (v2_z - v0_z)); // z3 = z0 + ((Dy01 / Dy12) * Dz02)

			// Ensure order of flat vectors (left -> right)
			if (v1_x > v3_x) { swap_vertex(v1_x, v1_y, v1_z, v3_x, v3_y, v3_z); }

			raster_flat_bot_triangle(v0_x, v0_y, v0_z, v1_x, v1_y, v1_z, v3_x,       v3_z);
			raster_flat_top_triangle(v1_x, v1_y, v1_z, v3_x,       v3_z, v2_x, v2_y, v2_z);
		}
	}

	template<class COLOR_BUFFER_TYPE>
	void Rasterizer<COLOR_BUFFER_TYPE>::raster_flat_top_triangle(
		float v0_x, float v0_y, float v0_z,
		float v1_x,				float v1_z,
		float v2_x, float v2_y, float v2_z
	) {
		// X CALCULATION
		float deltaSub = v2_y - v0_y;

		float slope0 = 0;
		float slope1 = 0;

		// Ensure valid division
		if (abs(deltaSub) > 0.001f) {
			slope0 = (v2_x - v0_x) / deltaSub;
			slope1 = (v2_x - v1_x) / deltaSub;
		}

		float x0 = v0_x;
		float x1 = v1_x;

		// Z CALCULATION  Zf = Z0 + t(Zdelta)
		float delta_z_0 = v2_z - v0_z;
		float delta_z_1 = v2_z - v1_z;

		float z_t = 0;
		float z_step = 1 / -deltaSub;
		
		// SCANLINE
		for (int scanlineY = (int)v0_y; scanlineY >= v2_y; scanlineY--) {
			raster_scanline(scanlineY, (int)x0, (int)x1, v0_z + z_t * delta_z_0, v1_z + z_t * delta_z_1);
			x0 -= slope0;
			x1 -= slope1;
			z_t += z_step;
		}
	}

	template<class COLOR_BUFFER_TYPE>
	void Rasterizer<COLOR_BUFFER_TYPE>::raster_flat_bot_triangle(
		float v0_x, float v0_y, float v0_z,
		float v1_x, float v1_y, float v1_z,
		float v2_x,		        float v2_z
	) {
		// X CALCULATION
		float deltaSub = v0_y - v1_y;

		float slope0 = 0;
		float slope1 = 0;

		// Ensure valid division
		if (abs(deltaSub) > 0.001f) {
			slope0 = (v0_x - v1_x) / deltaSub;
			slope1 = (v0_x - v2_x) / deltaSub;
		}

		float x0 = v0_x;
		float x1 = v0_x;

		// Z CALCULATION  Zf = Z0 + t(Zdelta)
		float delta_z_0 = v0_z - v1_z;
		float delta_z_1 = v0_z - v2_z;

		float z_t = 0;
		float z_step = 1 / deltaSub;

		// SCANLINE
		for (int scanlineY = (int)v0_y; scanlineY >= v1_y; scanlineY--) {
			raster_scanline(scanlineY, (int)x0, (int)x1, v0_z - z_t * delta_z_0, v0_z - z_t * delta_z_1);
			x0 -= slope0;
			x1 -= slope1;
			z_t += z_step;
		}
	}

	template<class COLOR_BUFFER_TYPE>
	void Rasterizer<COLOR_BUFFER_TYPE>::raster_scanline(const int y, int x0, const int x1, float z0, const float z1) {
		if (y < 0 || y >= (int)color_buffer.get_height()) return;

		float z_slope = (z1 - z0) / (x1 - x0);

		for (; x0 < x1; x0++) {
			if (x0 < 0 || x0 >= (int)color_buffer.get_width()) continue;

			int offset = y * color_buffer.get_width() + x0;
			if (z0 < z_buffer[offset]) {
				// If first time painting pixel, add to pixel buffer
				if (z_buffer[offset] == numeric_limits<int>::max()) {
					pixel_buffer[pixel_amount] = offset;
					pixel_amount++;
				}

				color_buffer.set_pixel(offset);
				z_buffer[offset] = (int)z0;
			}

			z0 += z_slope;
		}

		// Last pixel
		if (x1 < 0 || x1 >= (int)color_buffer.get_width()) return;
		int offset = y * color_buffer.get_width() + x1;
		if (z1 < z_buffer[offset]) {
			// If first time painting pixel, add to pixel buffer
			if (z_buffer[offset] == numeric_limits<int>::max()) {
				pixel_buffer[pixel_amount] = offset;
				pixel_amount++;
			}

			color_buffer.set_pixel(offset);
			z_buffer[offset] = (int)z1;
		}
	}
}

#endif
