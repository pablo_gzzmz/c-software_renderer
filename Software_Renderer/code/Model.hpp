

/*
	Author: Pablo Gonzalez Martinez

	Model class:
		- A model is a type of entity used to render loaded 3D models
		- Models will be added to the camera as a render vector to be rendered
		- They contain a vector of meshes
		- They will control the display-related transformation and perform render operations:
			- Front-face checking
			- Lighting based on normal for every triangle
			- Final color based on mesh color and ambient data
*/

#ifndef MODEL_HEADER
#define MODEL_HEADER

#include <vector>
#include "Mesh.hpp"
#include "Entity.hpp"
#include "Rasterizer.hpp"
#include "Color_Buffer_Rgba8888.hpp"
#include <Projection.hpp>
#include "Vector.hpp"
#include "Scene_Render_Info.hpp"

namespace engine {
	using std::vector;
	using namespace toolkit;
	typedef toolkit::Vector3f Vector3f;

	class Model : public Entity {
	private:
		vector<Mesh>	  meshes;

		// Data for rendering all meshes
		vector<Point4f> transformed_vertices;
		vector<Point3f> transformed_normals;
		vector<Point4i> display_vertices;

	public:
		Model(const std::string & obj_file_path,
			float _scale,
			float rotation_x,    float rotation_y,    float rotation_z,
			float translation_x, float translation_y, float translation_z);

		size_t get_vertices_number();
		size_t get_normals_number ();

		void render(const size_t							& width		 , 
			        const size_t							& height	 , 
			              Rasterizer<Color_Buffer_Rgba8888> * rasterizer , 
				    const Scene_Render_Info					& render_info);

		bool raster_setup(const Point4f * const vertices, const int * const indices, Color * color, const Scene_Render_Info render_info);

	private:
		int clamp(int i, int min, int max) {
			return i > max ? max : i < min ? min : i;
		}
	};
}

#endif