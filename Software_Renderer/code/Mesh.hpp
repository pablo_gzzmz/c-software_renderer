
/*
	Author: Pablo Gonzalez Martinez

	Mesh class:
		- A mesh is a group of object information that might share model with other meshes
		- It contains vertex, uv, normals and their indices
		- It also contains a color for the whole mesh
		- This data will be consulted by the parent model in order to render the object
*/

#ifndef MESH_HEADER
#define MESH_HEADER

#include "Point.hpp"
#include "Color_Buffer_Rgba8888.hpp"
#include <vector>

namespace engine {
	using std::vector;
	using toolkit::Point4f;
	using toolkit::Point3f;
	using toolkit::Point2f;

	typedef Color_Buffer_Rgba8888::Color Color;

	class Mesh {
	private:
		vector<Point4f> original_vertices;
		vector<Point2f> original_uvs;
		vector<Point3f> original_normals;
		vector<int>	    vertex_indices;
		vector<int>		uv_indices;
		vector<int>		normal_indices;

		Color mesh_color;

	public:
		vector<Point4f> & get_original_vertices() { return original_vertices; }
		vector<Point2f> & get_original_uvs	   () { return original_uvs;	  }
		vector<Point3f> & get_original_normals () { return original_normals;  }
		vector<int>     & get_vertex_indices   () { return vertex_indices;	  }
		vector<int>		& get_uv_indices	   () { return uv_indices;		  }
		vector<int>		& get_normal_indices   () { return normal_indices;	  }

		      Color & get_mesh_color()		 { return mesh_color; }
		const Color & get_mesh_color() const { return mesh_color; }
	};
}

#endif
