
/*
	Project Author: Pablo Gonzalez Martinez

	This project is a software renderer that loads objs and rasterizes them on screen.
	
	Features coded by the author:

	- Obj loader/parser to create models/meshes.
	- Rasterizer with flat top/bottom triangle division and z-buffer.
	- Scene graph
	- Lighting comparing tris' normals with a directional light and an ambient color addition.
	- Crappy AA 'simulation' imitating FXAA using edge detection via color contrast and gaussian blur.

	The cpp-toolkit has been provided by a third party.
	Color_Buffer code has been provided by a third party.

	This project should be compiled in Release mode, otherwise it will be too slow.

	Controls: 
		> A/D: rotates the objects around
		> P:   pauses the update
		> O:   changes the fake 'antialiasing' mode (none, blur, outline)

	*******************************************************************************************

	The project should be debugged on Release mode; Debug mode's overhead makes execution too slow

	*******************************************************************************************
*/

#include <SFML/Window.hpp>
#include <SFML/OpenGL.hpp>
#include <stdlib.h>
#include <time.h>
#include "Camera.hpp"
#include "Scene.hpp"

using namespace sf;
using namespace engine;

int main (){
	size_t window_width  = Screen::get_width();
	size_t window_height = Screen::get_height();

    Window window(VideoMode(window_width, window_height), "Pablo Gonzalez Martinez - Software Renderer", Style::Titlebar | Style::Close, ContextSettings(32));

	Clock timer;
	float delta_time = 0.017f; 

	srand((unsigned)time(NULL));

    // Initialization:
	Scene scene = Scene();

	Camera camera = Camera(5, 10, 20);
	scene.add_entity(&camera);

	Model landscape = Model("..\\..\\assets\\Landscape.obj", 0.07f, 0.5f, 180.f, 0.0f, 0.5f, -0.2f, -10.f);
	scene.add_entity (&landscape);
	Camera::add_model(&landscape);

	Model m1 = Model("..\\..\\assets\\test.obj", 0.15f, 0.f, 0.0f, 0.0f, -0.5f, 3.f, -10.f);
	landscape.add_child(&m1, "box");
	Camera::add_model(&m1);

	std::vector<Model> clouds;
	clouds.reserve(10);
	for (int n = 0; n < 5; n++) {
		float init_y = (float)(rand() % 1600 + 1400);
		init_y *= 0.01f;
		float init_scale = (float)(rand() % 55 + 20);
		init_scale *= 0.01f;

		clouds.push_back(Model("..\\..\\assets\\Cloud.obj", init_scale, 0.0f, 90.0f, 0.0f, -(float)n * 20, init_y, -10 + -(float)n*10));
		landscape.add_child(&clouds[n], "cloud"+n);
		Camera::add_model(&clouds[n]);
	}

	Model tree = Model("..\\..\\assets\\Tree.obj", 0.04f, 0.f, 0.0f, 0.0f, 10.f, 5.f, 0.f);
	landscape.add_child(&tree, "tree1");
	Camera::add_model(&tree);

	Model tree2 = Model("..\\..\\assets\\Tree.obj", 0.04f, 0.f, 0.0f, 0.0f, 16.f, 6.f, 1.f);
	landscape.add_child(&tree2, "tree2");
	Camera::add_model(&tree2);

	Model tree3 = Model("..\\..\\assets\\Tree.obj", 0.04f, 0.1f, 0.0f, 0.0f, 20.f, 6.f, 2.f);
	landscape.add_child(&tree3, "tree3");
	Camera::add_model(&tree3);

	Model tree4 = Model("..\\..\\assets\\Tree.obj", 0.04f, 0.f, 0.0f, 0.0f, 14.f, 5.f, -1.f);
	landscape.add_child(&tree4, "tree4");
	Camera::add_model(&tree4);

	Model windmill_base = Model("..\\..\\assets\\Windmill_base.obj", 1.f, 0.f, 1.0f, 0.0f, 10.f, 3.5f, -5.f);
	landscape.add_child(&windmill_base, "windmill");
	Camera::add_model(&windmill_base);

	Model windmill_sub = Model("..\\..\\assets\\Windmill_sub.obj", 1.f, 0.0f, 0.0f, 0.0f, 1.7f, 4.1f, 0.f);
	windmill_base.add_child(&windmill_sub, "Blades");
	Camera::add_model(&windmill_sub);

    window.setVerticalSyncEnabled (true);

    glDisable (GL_BLEND);
    glDisable (GL_DITHER);
    glDisable (GL_CULL_FACE);
    glDisable (GL_DEPTH_TEST);
    glDisable (GL_TEXTURE_2D);

    glViewport     (0, 0, window_width, window_height);
    glMatrixMode   (GL_PROJECTION);
    glLoadIdentity ();
    glOrtho        (0, GLdouble(window_width), 0, GLdouble(window_height), -1, 1);

	int alias_count = 0;
	bool update = true;

    bool running = true;
    do {
		timer.restart();

		Event event;

		while (window.pollEvent(event)) {
			switch (event.type) {
			case Event::Closed:
				running = false;
				break;
			case Event::KeyPressed:
				switch (event.key.code) {
				case sf::Keyboard::P:
					update = !update;
					break;
				case sf::Keyboard::O:
					switch (alias_count) {
					case 0:
						alias_count++;
						Camera::get_main()->set_display_aliasing(true);
						break;
					case 1:
						alias_count++;
						Antialiasing::debugging_switch();
						break;
					case 2:
						alias_count = 0;
						Camera::get_main()->set_display_aliasing(false);
						Antialiasing::debugging_switch();
						break;
					}
					break;
				}
				break;

			case Event::KeyReleased:
				switch (event.key.code) {
				case sf::Keyboard::A:
					landscape.add_rotation_y(-0.5f);
					break;
				case sf::Keyboard::D:
					landscape.add_rotation_y(0.5f);
					break;
				}
				break;
			}
		}

		if (update) {
			// Update
			landscape.add_rotation_y(0.01f * delta_time);

			m1.add_rotation_y(0.5f * delta_time);

			windmill_sub.add_rotation_x(0.5f * delta_time);

			for (auto it = clouds.begin(); it != clouds.end(); it++) {
				it->add_translation(delta_time * 0.5f, 0, delta_time * 0.2f);
			}

			scene.update(delta_time);
		}

		// Render
		Camera::get_main()->render();
        window.display ();

		delta_time = (delta_time + timer.getElapsedTime().asSeconds()) * 0.5f;
    }
    while (running);

    return (EXIT_SUCCESS);
}
