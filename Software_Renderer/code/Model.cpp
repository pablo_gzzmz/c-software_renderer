
#include <cmath>
#include "Model.hpp"
#include "Obj_Loader.hpp"
#include <iostream>
#include <assert.h>

namespace engine {
	typedef Color_Buffer_Rgba8888::Color Color;

	Model::Model(const std::string & obj_file_path,
		float _scale,
		float _rotation_x, float _rotation_y, float _rotation_z,
		float _translation_x, float _translation_y, float _translation_z) 
		: Entity(_scale, _rotation_x, _rotation_y, _rotation_z, _translation_x, _translation_y, _translation_z) {
		bool success = load_obj(&meshes, obj_file_path);

		assert(success);

		size_t n = get_vertices_number();

		transformed_vertices.resize(n);
		display_vertices.resize(n);
	}

	size_t Model::get_vertices_number() {
		size_t n = 0;
		vector<Mesh>::iterator it = meshes.begin();
		for (; it != meshes.end(); it++) {
			n += it->get_original_vertices().size();
		}
		return n;
	}

	size_t Model::get_normals_number() {
		size_t n = 0;
		vector<Mesh>::iterator it = meshes.begin();
		for (; it != meshes.end(); it++) {
			n += it->get_original_normals().size();
		}
		return n;
	}

	void Model::render(const size_t							   & width      ,
					   const size_t							   & height     ,
					         Rasterizer<Color_Buffer_Rgba8888> * rasterizer ,
					   const Scene_Render_Info                 & render_info){

		// Display transformation matrices
		Translation3f display_translation = Translation3f(float(width / 2), float(height / 2), 1.f);
		Scaling3f	  display_scaling     = Scaling3f    (float(width / 2), float(height / 2), 100000000.f);

		Transformation3f display_transformation = display_translation * display_scaling;

		// All rendering vertices transformation
		size_t total_index = 0;
		for (auto it = meshes.begin(); it != meshes.end(); it++) {
			for (size_t index = 0, n = it->get_original_vertices().size(); index < n; index++) {
				Point4f & vertex = transformed_vertices[total_index] = Matrix44f(transform) * Matrix41f(it->get_original_vertices()[index]);

				// Normalize
				float divisor = 1.f / vertex[3];
				vertex[0] *= divisor;
				vertex[1] *= divisor;
				vertex[2] *= divisor;
				vertex[3] = 1.f;

				display_vertices[total_index] = Point4i(Matrix44f(display_transformation) * Matrix41f(transformed_vertices[total_index]));
				total_index++;
			}

			// For every face in every mesh
			for (int * indices = it->get_vertex_indices().data(), *end = indices + it->get_vertex_indices().size(); indices < end; indices += 3) {
				Color render_color = it->get_mesh_color();

				// Rasterize
				if (raster_setup(transformed_vertices.data(), indices, &render_color, render_info)) {
					rasterizer->set_color(render_color);

					rasterizer->raster_triangle(display_vertices.data(), indices);
				}
			}
		}
	}

	bool Model::raster_setup(const Point4f * const vertices, const int * const indices, Color * color, const Scene_Render_Info render_info) {
		const Point4f & v0 = vertices[indices[0]];
		const Point4f & v1 = vertices[indices[1]];
		const Point4f & v2 = vertices[indices[2]];

		Vector3f a = Vector3f({ v1[0] - v0[0], v1[1] - v0[1], 0 });
		Vector3f b = Vector3f({ v2[0] - v0[0], v2[1] - v0[1], 0 });

		// Check if front face
		if (a[0] * b[1] - b[0] * a[1] > 0.f) return false;

		// If successful front face checking, establish remaining values of both vectors (z)
		a[2] = v1[2] - v0[2];
		b[2] = v2[2] - v0[2];

		// Normal of face
		float normal_0 = a[1] * b[2] - a[2] * b[1];
		float normal_1 = a[2] * b[0] - a[0] * b[2];
		float normal_2 = a[0] * b[1] - a[1] * b[0];

		float float_var;

		// Normal magnitude
		float_var = sqrt((normal_0 * normal_0) + (normal_1 * normal_1) + (normal_2 * normal_2));

		// Normalize
		normal_0 = normal_0 / float_var;
		normal_1 = normal_1 / float_var;
		normal_2 = normal_2 / float_var;

		// Dot product between normal and light
		float_var = normal_0 * render_info.light[0] + normal_1 * render_info.light[1] + normal_2 * render_info.light[2];

		// Set value to 0 if facing opposite to light
		if (float_var < 0) {
			float_var = 0;
		}
		else {
			// Lambert cos
			float_var = cos(acos(float_var));
		}

		// For simplicity we add an ambient intensity to all models
		static const float ambient_intensity = 0.5f;

		color->set(
			clamp((int)(color->R() * float_var + render_info.ambient_color.R() * ambient_intensity), 0, 255),
			clamp((int)(color->G() * float_var + render_info.ambient_color.G() * ambient_intensity), 0, 255),
			clamp((int)(color->B() * float_var + render_info.ambient_color.B() * ambient_intensity), 0, 255)
		);

		return true;
	}
}