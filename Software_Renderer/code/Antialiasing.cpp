
#include "Antialiasing.hpp"

namespace engine {
	bool Antialiasing::debugging_edges = false;

	void Antialiasing::pass() {
		std::vector<Color> & buffer = Screen::get_rasterizer()->get_color_buffer().get_buffer();
		swap_buffer = buffer;

		size_t pitch = Screen::get_width();
		size_t pitch_minus_one = pitch - 1;
		size_t bot_check = buffer.size() - pitch;

		mask_count = 0;

		for (size_t count = 0; count < Screen::get_rasterizer()->get_pixel_amount(); count++) {
			size_t offset = Screen::get_rasterizer()->get_pixel_buffer()[count];

			pixel_hztl_pos check_hztl = HZTL_DEFAULT;
			pixel_vtcl_pos check_vtcl = VTCL_DEFAULT;

			float mod = (float)(offset % pitch);
			if (mod == 0)			    check_hztl = LEFT_BORDER;
			else
			if (mod == pitch_minus_one) check_hztl = RIGHT_BORDER;

			if (offset >= bot_check)    check_vtcl = BOTTOM_BORDER;
			else
			if (offset < pitch)			check_vtcl = TOP_BORDER;

			Color main_color = buffer[offset];

			size_t sum = 0;

			// Get neighbouring colors considering position of offset
			switch (check_hztl) {
			case LEFT_BORDER:
				switch (check_vtcl) {
				case TOP_BORDER:
					mask_count = 2;
					mask[0] = buffer[offset + 1];
					sum = offset + pitch;
					mask[1] = buffer[sum];
					diagonal_mask[0] = buffer[sum + 1];
					break;

				case BOTTOM_BORDER:
					mask_count = 2;
					mask[0] = buffer[offset + 1];
					sum = offset - pitch;
					mask[1] = buffer[sum];
					diagonal_mask[0] = buffer[sum + 1];
					break;

				default:
					mask_count = 3;
					mask[0] = buffer[offset + 1];
					sum = offset + pitch;
					mask[1] = buffer[sum];
					diagonal_mask[0] = buffer[sum + 1];
					sum = offset - pitch;
					mask[2] = buffer[sum];
					diagonal_mask[1] = buffer[sum + 1];
					break;
				}
				break;

			case RIGHT_BORDER:
				switch (check_vtcl) {
				case TOP_BORDER:
					mask_count = 2;
					mask[0] = buffer[offset - 1];
					sum = offset + pitch;
					mask[1] = buffer[sum];
					diagonal_mask[sum - 1];
					break;

				case BOTTOM_BORDER:
					mask_count = 2;
					mask[0] = buffer[offset - 1];
					sum = offset - pitch;
					mask[1] = buffer[sum];
					diagonal_mask[sum - 1];
					break;

				default:
					mask_count = 3;
					mask[0] = buffer[offset - 1];
					sum = offset + pitch;
					mask[1] = buffer[sum];
					diagonal_mask[0] = buffer[sum - 1];
					sum = offset - pitch;
					mask[2] = buffer[sum];
					diagonal_mask[1] = buffer[sum - 1];
					break;
				}
			default:
				switch (check_vtcl) {
				case TOP_BORDER:
					mask_count = 3;
					mask[0] = buffer[offset - 1];
					mask[1] = buffer[offset + 1];
					sum = offset + pitch;
					mask[2] = buffer[sum];
					diagonal_mask[0] = buffer[sum + 1];
					diagonal_mask[1] = buffer[sum - 1];
					break;

				case BOTTOM_BORDER:
					mask_count = 3;
					mask[0] = buffer[offset - 1];
					mask[1] = buffer[offset + 1];
					sum = offset - pitch;
					mask[2] = buffer[sum];
					diagonal_mask[0] = buffer[sum + 1];
					diagonal_mask[1] = buffer[sum - 1];
					break;

				default:
					mask_count = 4;
					mask[0] = buffer[offset - 1];
					mask[1] = buffer[offset + 1];
					sum = offset - pitch;
					mask[2] = buffer[sum];
					diagonal_mask[0] = buffer[sum + 1];
					diagonal_mask[1] = buffer[sum - 1];
					sum = offset + pitch;
					mask[3] = buffer[sum];
					diagonal_mask[2] = buffer[sum + 1];
					diagonal_mask[3] = buffer[sum - 1];
					break;
				}
				break;
			}

			int final_r = main_color.R();
			int final_g = main_color.G();
			int final_b = main_color.B();
			int add_r = 0;
			int add_g = 0;
			int add_b = 0;

			int valid = 0;
			int dif = 0;

			switch (mask_count) {
			case 2:
				// Local contrast calculation - edge finding
				dif = color_diff(main_color, mask[0]);
				if (dif > edge_threshold)
					valid++;
				dif = color_diff(main_color, mask[1]);
				if (dif > edge_threshold)
					valid++;

				// If no edges found, ignore
				if (valid == 0) continue;

				// Gaussian blur
				add_r  = mask[0].R() + mask[1].R();
				add_r *= 2;
				add_r += diagonal_mask[0].R();
				add_g  = mask[0].G() + mask[1].G();
				add_g *= 2;
				add_g += diagonal_mask[0].G();
				add_b  = mask[0].B() + mask[1].B();
				add_b *= 2;
				add_b += diagonal_mask[0].B();
				break;

			case 3:
				// Local contrast calculation - edge finding
				dif = color_diff(main_color, mask[0]);
				if (dif > edge_threshold)
					valid++;
				dif = color_diff(main_color, mask[1]);
				if (dif > edge_threshold)
					valid++;
				dif = color_diff(main_color, mask[2]);
				if (dif > edge_threshold)
					valid++;

				// If no edges found, ignore
				if (valid == 0) continue;

				// Gaussian blur
				add_r  = mask[0].R() + mask[1].R() + mask[2].R();
				add_r *= 2;
				add_r += diagonal_mask[0].R() + diagonal_mask[1].R();
				add_g  = mask[0].G() + mask[1].G() + mask[2].G();
				add_g *= 2;
				add_g += diagonal_mask[0].G() + diagonal_mask[1].G();
				add_b  = mask[0].B() + mask[1].B() + mask[2].B();
				add_b *= 2;
				add_b += diagonal_mask[0].B() + diagonal_mask[1].B();
				break;

			case 4:
				// Local contrast calculation - edge finding
				dif = color_diff(main_color, mask[0]);
				if (dif > edge_threshold)
					valid++;
				dif = color_diff(main_color, mask[1]);
				if (dif > edge_threshold)
					valid++;
				dif = color_diff(main_color, mask[2]);
				if (dif > edge_threshold)
					valid++;
				dif = color_diff(main_color, mask[3]);
				if (dif > edge_threshold)
					valid++;

				// If no edges found, ignore
				if (valid == 0) continue;

				// Gaussian blur
				add_r  = mask[0].R() + mask[1].R() + mask[2].R() + mask[3].R();
				add_r *= 2;
				add_r += diagonal_mask[0].R() + diagonal_mask[1].R() + diagonal_mask[2].R() + diagonal_mask[3].R();
				add_g  = mask[0].G() + mask[1].G() + mask[2].G() + mask[3].G();
				add_g *= 2;
				add_g += diagonal_mask[0].G() + diagonal_mask[1].G() + diagonal_mask[2].G() + diagonal_mask[3].G();
				add_b  = mask[0].B() + mask[1].B() + mask[2].B() + mask[3].B();
				add_b *= 2;
				add_b += diagonal_mask[0].B() + diagonal_mask[1].B() + diagonal_mask[2].B() + diagonal_mask[3].B();
				break;
			}

			if (debugging_edges) {
				final_r = 0;
				final_g = 0;
				final_b = 0;
			}
			else {
				// Anti-Alias = Crappy gaussian blur
				final_r = (int)((final_r * 4 + add_r) * 0.0625f); //0.0625f = 1 / 16 (Gaussian blur multiplier)
				final_r = final_r > 255 ? 255 : final_r;
				final_g = (int)((final_g * 4 + add_g) * 0.0625f);
				final_g = final_g > 255 ? 255 : final_g;
				final_b = (int)((final_b * 4 + add_b) * 0.0625f);
				final_b = final_b > 255 ? 255 : final_b;
			}

			swap_buffer[offset] = Color(final_r, final_g, final_b, 255);
		}

		buffer = swap_buffer;
	}
}