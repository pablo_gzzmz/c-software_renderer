

/*
	Author: Pablo Gonzalez Martinez

	Scene render info struct:
		- Contains the basic data to be passed to a model for its rendernig
			- A directional light
			- An ambient color
*/

#ifndef SCENE_RENDER_INFO_HEADER
#define SCENE_RENDER_INFO_HEADER

#include "Vector.hpp"
#include "Color_Buffer_Rgba8888.hpp"

namespace engine {
	typedef Color_Buffer_Rgba8888::Color Color;

	struct Scene_Render_Info {
		// Directional light
		toolkit::Vector3f light;

		// Ambient color
		Color ambient_color;

		Scene_Render_Info(toolkit::Vector3f _light, Color _ambient_color) {
			light = _light;
			ambient_color = _ambient_color;
		}
	};
}

#endif

