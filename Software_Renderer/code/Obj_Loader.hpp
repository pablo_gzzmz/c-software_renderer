

/*
	Author: Pablo Gonzalez Martinez

	Obj loader class:
		- Loads an obj through a file path
		- Recieves a pointer to vector of meshes to save loaded data
		- Saves vector, normal and texture coordinates
		- Divides loaded obj in different meshes and assigns a read color to the mesh
*/

#ifndef  OBJ_LOADER_HEADER
#define OBJ_LOADER_HEADER

#include <string>
#include <vector>
#include "Mesh.hpp"

namespace engine {
	bool load_obj(vector<Mesh> * meshes, const std::string & obj_file_path);
}

#endif
