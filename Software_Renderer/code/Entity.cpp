
#include "Entity.hpp"

namespace engine {
	Entity::Entity() {
		scale = 1;

		rotation_x = 0.0f;
		rotation_y = 0.0f;
		rotation_z = 0.0f;

		translation_x = 0;
		translation_y = 0;
		translation_z = 0;
	}

	Entity::Entity(float _scale,
				   float _rotation_x,    float _rotation_y,    float _rotation_z,
				   float _translation_x, float _translation_y, float _translation_z
		){
		scale = _scale;

		rotation_x = _rotation_x;
		rotation_y = _rotation_y;
		rotation_z = _rotation_z;

		translation_x = _translation_x;
		translation_y = _translation_y;
		translation_z = _translation_z;
	}
}