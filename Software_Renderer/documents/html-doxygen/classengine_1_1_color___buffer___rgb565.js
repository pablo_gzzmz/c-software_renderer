var classengine_1_1_color___buffer___rgb565 =
[
    [ "Color", "structengine_1_1_color___buffer___rgb565_1_1_color.html", "structengine_1_1_color___buffer___rgb565_1_1_color" ],
    [ "Buffer", "classengine_1_1_color___buffer___rgb565.html#a3ea369d11789f3abf3dd08b949eb9e26", null ],
    [ "Color_Buffer_Rgb565", "classengine_1_1_color___buffer___rgb565.html#a01150c5ba55e55d4275d247f559ed319", null ],
    [ "bits_per_color", "classengine_1_1_color___buffer___rgb565.html#a65d37b1c6ebc1f098997ca2a375638a1", null ],
    [ "colors", "classengine_1_1_color___buffer___rgb565.html#ab8b228b0d12541e38fb25fd91faf55a6", null ],
    [ "colors", "classengine_1_1_color___buffer___rgb565.html#ab3c58237045bbd44ca4c736428e5e8ef", null ],
    [ "gl_draw_pixels", "classengine_1_1_color___buffer___rgb565.html#a3a581ac8f275295f9b28ba5119a445a5", null ],
    [ "set_color", "classengine_1_1_color___buffer___rgb565.html#ac967e6bdcabc7f1b2f3b8f2f9288e134", null ],
    [ "set_color", "classengine_1_1_color___buffer___rgb565.html#a637916c888632f091f0ae7ed35c28887", null ],
    [ "set_pixel", "classengine_1_1_color___buffer___rgb565.html#a85900714cc2351e3bc34a64c8b019299", null ],
    [ "set_pixel", "classengine_1_1_color___buffer___rgb565.html#aa6716f31af98711ee75b5ce7428e1be6", null ],
    [ "size", "classengine_1_1_color___buffer___rgb565.html#ac1f1ea663980a661bf235ff05fb18ff3", null ]
];