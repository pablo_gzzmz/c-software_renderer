var classengine_1_1_mesh =
[
    [ "get_mesh_color", "classengine_1_1_mesh.html#ae228d4536e3f51959c2c1439e15794f1", null ],
    [ "get_mesh_color", "classengine_1_1_mesh.html#aad99cb336d0e0abc6c529edd42aafb16", null ],
    [ "get_normal_indices", "classengine_1_1_mesh.html#a9835e9f800d7e6932f3678fa64687bc6", null ],
    [ "get_original_normals", "classengine_1_1_mesh.html#aa3c7d9be7bd3054c80bbe789e3fe7c73", null ],
    [ "get_original_uvs", "classengine_1_1_mesh.html#a4abe5d83150f2a587b84c6e95da2112c", null ],
    [ "get_original_vertices", "classengine_1_1_mesh.html#ad5485f9637eb5af8a2fec5bf1b5b5810", null ],
    [ "get_uv_indices", "classengine_1_1_mesh.html#a588f93f2180a8e192222fdac8e6118cd", null ],
    [ "get_vertex_indices", "classengine_1_1_mesh.html#a1d97e97ea51054347b47d9dfd8aeb24c", null ]
];