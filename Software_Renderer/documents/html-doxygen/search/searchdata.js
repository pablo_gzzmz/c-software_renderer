var indexSectionsWithContent =
{
  0: "abcdeghilmoprstuvw",
  1: "acemrs",
  2: "e",
  3: "acemors",
  4: "abcdegilmoprsu",
  5: "abcdghlrstvw",
  6: "bcv"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs"
};

