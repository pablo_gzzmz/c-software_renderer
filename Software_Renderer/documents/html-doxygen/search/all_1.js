var searchData=
[
  ['b',['b',['../structengine_1_1_color___buffer___rgba8888_1_1_color.html#a9dbc65a20fea2faca378fa63293bec26',1,'engine::Color_Buffer_Rgba8888::Color::b()'],['../structengine_1_1_color___buffer___rgba8888_1_1_color.html#afa112e938fb30121f50a04a11c25ff35',1,'engine::Color_Buffer_Rgba8888::Color::B() const']]],
  ['bits_5fper_5fcolor',['bits_per_color',['../classengine_1_1_color___buffer.html#a24b8b18c57932f69080cd9478af83fff',1,'engine::Color_Buffer::bits_per_color()'],['../classengine_1_1_color___buffer___rgb565.html#a65d37b1c6ebc1f098997ca2a375638a1',1,'engine::Color_Buffer_Rgb565::bits_per_color()'],['../classengine_1_1_color___buffer___rgba8888.html#aab53f47a612e784af31f245c87fde6f7',1,'engine::Color_Buffer_Rgba8888::bits_per_color()']]],
  ['buffer',['Buffer',['../classengine_1_1_color___buffer___rgb565.html#a3ea369d11789f3abf3dd08b949eb9e26',1,'engine::Color_Buffer_Rgb565']]]
];
